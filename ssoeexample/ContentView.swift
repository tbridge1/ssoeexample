//
//  ContentView.swift
//  ssoeexample
//
//  Created by Joel Rennich on 6/4/20.
//  Copyright © 2020 Jamf. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("This app doesn't do anything!")
            .frame(maxWidth: .infinity, maxHeight: .infinity)
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
